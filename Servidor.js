 var http_port = 8080;
 var http = require('http');
  
 var server = http.createServer( function(req, res)
 {   var url = require('url');
    req.requrl = url.parse(req.url, true);

    req.a = (req.requrl.query.a && (!isNaN(req.requrl.query.a)) ? new Number(req.requrl.query.a):NaN);
    req.b = (req.requrl.query.b && (!isNaN(req.requrl.query.b)) ? new Number(req.requrl.query.b):NaN);


    if(req.requrl.pathname === '/')
    {
        res.writeHead(200, {'Content-Type' : 'text/html'});
        res.end(["<ul><li><a href='/suma'>suma</a></li></ul>",
                 "<ul><li><a href='/resta'>resta</a></li></ul>",
                 "<ul><li><a href='/multiplicacio'>multiplicacio</a></li></ul>",
                 "<ul><li><a href='/divisio'>divisio</a></li></ul>"
    ].join("\n"));
    }
    else if(req.requrl.pathname === '/suma')
    {
        require('./Suma').get(req,res);
        
    }
    else if(req.requrl.pathname === '/resta')
    {
        require('./Resta').get(req,res);
    }
    else if(req.requrl.pathname === '/multiplicacio')
    {
        require('./Multp').get(req,res);
    }
    else if(req.requrl.pathname === '/divisio')
    {
        require('./Div').get(req,res);
    }
    else
    {
        res.writeHead(404, {'Content-Type' : 'text(html'});
        res.end('url incorrecta');
    }
 }
 )

 server.listen(http_port);
 console.log('Escoltant a http://localhost:' + http_port);
